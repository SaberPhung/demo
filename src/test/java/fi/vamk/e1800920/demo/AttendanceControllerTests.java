package fi.vamk.e1800920.demo;


import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;


import java.time.Instant;

import org.apache.commons.collections4.IterableUtils;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@Configuration
@ComponentScan(basePackages={"fi.vamk.e1800920.demo"})
@EnableJpaRepositories(basePackageClasses = AttendanceRepository.class)


public class AttendanceControllerTests {
    @Autowired
    private AttendanceRepository repository;


    @Test
    public void postGetDeleteAttendance(){
        Iterable<Attendance> begin = repository.findAll();
        System.out.println(IterableUtils.size(begin));
        //given
        Attendance att= new Attendance("ABCD");
        System.out.println("ATT: "+ att.toString());
        //test save
        Attendance saved = repository.save(att);
        System.out.println("SAVED: " + saved.toString());
        //when
        Attendance found = repository.findByKey(att.getKey());  
        System.out.println("FOUND: "+ found.toString());
        //then
        assertThat(found.getKey()).isEqualTo(att.getKey());  
        repository.delete(found);
        Iterable<Attendance> end = repository.findAll();
        System.out.println(IterableUtils.size(end));
        assertEquals((long)IterableUtils.size(begin),(long)IterableUtils.size(end));
    }
    @Test
    public void addFetchByDate() {
    Attendance a = new Attendance("uhuhu");
    a.setDate(Instant.parse("2010-10-02T12:23:23Z"));
    System.out.println("a1 " + a.toString());
    Attendance saved = repository.save(a);
    System.out.println("save " + saved.toString());
    Attendance found = repository.findByDate(Instant.parse("2010-10-02T12:23:23Z"));
    System.out.println("found " + found.toString());
    assertEquals(a.toString(), found.toString());
  }
}
