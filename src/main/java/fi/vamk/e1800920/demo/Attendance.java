package fi.vamk.e1800920.demo;

import java.io.Serializable;
import java.time.Instant;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.NamedQuery;

@Entity
@NamedQuery(name="Attendance.findAll",query="SELECT p FROM Attendance p")
public class Attendance implements Serializable{
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String key;
    private Instant date;

    public Attendance(String key) {
        date = Instant.now();
        this.key = key;
      }
   
    public Attendance(){
        
    }
    
    public int getId() {
        return this.id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getKey() {
        return this.key;
    }
    public void setKey(String key) {
        this.key = key;
    }
    public Instant getDate() {
        return this.date;
    }
    public void setDate(Instant date) {
        this.date = date;
    }
    public String toString() {
        return "Attendance" +"Date=" + date.toString() +", id=" + id +", key='" + key + '\'' +'}';
        //return id + " " + key + " " + date.toString();
    }
}
