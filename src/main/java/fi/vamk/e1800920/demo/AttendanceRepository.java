package fi.vamk.e1800920.demo;

import org.springframework.data.repository.CrudRepository;
import java.time.Instant;

public interface AttendanceRepository extends CrudRepository <Attendance, Integer>{
    public Attendance findByKey(String key);
    public Attendance findByDate(Instant date);
}
